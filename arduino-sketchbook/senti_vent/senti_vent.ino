/*
  ================
  SENTI VENTILATOR
  ================
  
  Senti's rapid ventilator prototype for the Covid-19 pandemic effort.
  
  This program is written for the Arduino Uno, with an LCD Keypad Shield, 
  though only the LCD is used as of yet.
  
  Additionally, a 10k potentiometer is connected with the wiper on pin A1,
  with ends to +5v and ground.
  
  Pin D2 is connected to the "IN1" pin of a 5v relay module.
  
  LCD keypad shield pinout:
    * LCD RS pin to digital pin 8
    * LCD Enable pin to digital pin 9
    * LCD D4 pin to digital pin 4
    * LCD D5 pin to digital pin 5
    * LCD D6 pin to digital pin 6
    * LCD D7 pin to digital pin 7
    * LCD R/W pin to ground
    * LCD VSS pin to ground
    * LCD VCC pin to 5V
    * 10K resistor:
    * ends to +5V and ground
    * wiper to LCD VO pin (pin 3)

*/

// Include libraries:
#include <LiquidCrystal.h>

// Initialize LiquidCrystal library with the numbers of the interface pins:
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

// Assign pins
const int Valve = 2;
const int bpmPotPin = A1;
  
// Assign variables
float bpmInput = 0;
int bpmValue = 10;
unsigned long interval = 1000;
unsigned long counter = millis();
  
void setup() {
  // Set up the LCD with correct number of columns and rows 
  lcd.begin(16, 2);

  // Print header to the LCD.
  lcd.print("BPM     INTERVAL");

  // Initialize outputs  
  pinMode(Valve, OUTPUT); 

  // Short delay to ensure outputs aren't fired before controller is stable
  delay(4000);
}

void loop() {
  // Read potentiometer & calculate interval  
  bpmInput = analogRead(bpmPotPin);
  bpmValue = ((bpmInput / 1023) * 22) + 10;
  interval = (60000UL / bpmValue) / 3; 

  // Write to LCD
  lcd.setCursor(0, 1);
  lcd.print(bpmValue);
  lcd.setCursor(8,1);
  lcd.print(interval);

  // Toggle valves, 1:2 inspiration:expiration period
  if ((digitalRead(Valve) == LOW) && (millis() >= counter)){
    digitalWrite(Valve, HIGH);
    counter = millis() + (2 * interval);
  }

  if ((digitalRead(Valve) == HIGH) && (millis() >= counter)){
    digitalWrite(Valve, LOW);
    counter = millis() + interval;
  }
}
