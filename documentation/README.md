# Documentation

This directory contains all the prepared documentation for the Senti Ventilator.

1. [Ventilator Overview](overview.md)
2. [Manufacturing](manufacturing.md)
3. [Design Decision Justifications](design-decisions.md)
