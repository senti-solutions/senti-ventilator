# Manufacturing

The machine’s vessels and tubing are made from sanitary parts common to the pharmaceutical, dairy, brewing and cannabis industry. These were selected due to their availability and versatility. The two types of fitting used are the tri clamp, for the vessels, and compression for the lines as this allows the most adaptability.

## Frame

The frame is made by welding 1” stainless square tubing stock that is ground and polished for cleanability.
