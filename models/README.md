# Models

This directory contains all the prepared 3D models of the ventilator. Currently there is only a single model prepared.

- [VENT-ASM-FULL.STEP](VENT-ASM-FULL.STEP) - Model for the main structure of the ventilator.
