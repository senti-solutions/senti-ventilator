# Arduino Sketchbook

This directory contains an Arduino sketchbook containing a sketch (`senti_vent`) with the program used to run the ventilator.

## Sketch Details

This program is written for the Arduino Uno, with an LCD Keypad Shield, though only the LCD is used as of yet.

Additionally, a 10k potentiometer is connected with the wiper on pin A1, with ends to +5v and ground.

Pin D2 is connected to the "IN1" pin of a 5v relay module.

LCD keypad shield pinout:

* LCD RS pin to digital pin 8
* LCD Enable pin to digital pin 9
* LCD D4 pin to digital pin 4
* LCD D5 pin to digital pin 5
* LCD D6 pin to digital pin 6
* LCD D7 pin to digital pin 7
* LCD R/W pin to ground
* LCD VSS pin to ground
* LCD VCC pin to 5V
* 10K resistor:
* ends to +5V and ground
* wiper to LCD VO pin (pin 3)

## Running Locally

In order to get a copy of this project to work on yourself, you can create zip folder of the contents of this directory and then [import](https://create.arduino.cc/projecthub/Arduino_Genuino/import-your-sketchbook-and-libraries-to-the-web-editor-296bb3) it into the [Arduino Web Editor](https://create.arduino.cc/editor).

If using the [Arduino IDE](https://www.arduino.cc/en/Main/Software#download), you can also copy the contents of this folder into your sketchbook folder (On Windows for example, this is `C:\Users\<username>\Documents\Arduino` by default).

## Credits

Library originally added 18 Apr 2008<br>
by David A. Mellis<br>

Library modified 5 Jul 2009<br>
by Limor Fried (http://www.ladyada.net)<br>

Example added 9 Jul 2009<br>
by Tom Igoe<br>

Modified 22 Nov 2010<br>
by Tom Igoe

Thanks to Nick Gammon (www.gammon.com.au) for his Arduino tutorials.