# Senti Ventilator

Senti's rapid ventilator prototype for the Covid-19 pandemic effort.

You can see an [overview video](https://www.youtube.com/watch?v=Q0JNdcl2QMc) of the project on YouTube.

## Repository Contents

The following folders are contained in this repository:

1. [Documentation](documentation/README.md) - All the prepared documentation for the Senti Ventilator

    1.1. [Ventilator Overview](documentation/overview.md)

    1.2. [Manufacturing](documentation/manufacturing.md)

    1.3. [Design Decision Justifications](documentation/design-decisions.md)

2. [Models](models/README.md) - 3D models of the ventilator.

3. [Arduino Sketchbook](arduino-sketchbook/README.md) - The Arduino code used to run the Ventilator.

## Licensing

Please see the included [license file](UNLICENSE.txt).
