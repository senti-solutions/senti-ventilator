# Design Decision Justifications

## Certifiability

### Concerns

Many of the requirements laid out by the government are not realistic to achieve in a useful timeframe if mass production is to be possible. The main hurdle being the sensor requirements for some of the functionalities, as well as following standards for touchscreen compliance etc.
Additionally by attempting to comply with some of the functionality requirements the systems will become more prone to failure.

### Proposed Solution

The systems should be as simple as possible, with minimum reliance on control systems and electronics. The focus should be on meeting the lifesaving criteria while preventing further spread of the virus. Instead of having robust control systems on each machine, sensors and calibration equipments that are in short supply should be used to verify the functionality of these stable machines. After basic needs have been met then control systems can be added.

## Useability

### Concerns

Each function that is added to the system must be explained to the operator, this incurs a chance of failure at each layer of complication. There comes a point when safety features actually increase risk.

### Proposed Solution

## Reliability

### Concerns

### Proposed Solution

## Manufacturing Maneuverability

### Concerns

### Proposed Solution

## Manufacturing Capacities

### Concerns

### Proposed Solution

## Supply Chain Management

### Concerns

### Proposed Solution

## Resource Allocation

### Concerns

### Proposed Solution

## Cleanability

### Concerns

### Proposed Solution

## Servicing

### Concerns

In typical equipment servicing; reduced access to tools/utilities, limited space and stressed clients make a well developed system difficult to repair. Add social distancing, the chaos of a hospital during a pandemic and 100k units rushed through testing to be shipped across the globe.

### Proposed Solution

